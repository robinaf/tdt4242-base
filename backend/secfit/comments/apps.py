from django.apps import AppConfig


class CommentsConfig(AppConfig):
    """AppConfig for comments app.

    Attributes:
        name (str): The name of the application
    """

    name = "comments"
