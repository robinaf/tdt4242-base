"""Serializers for the workouts application."""
from rest_framework import serializers

from workouts.models import (
    Exercise,
    ExerciseInstance,
    ExerciseUnit,
    ExerciseUnitInstance,
    RememberMe,
    Workout,
    WorkoutFile,
)


class ExerciseUnitSerializer(serializers.ModelSerializer):

    class Meta:
        model = ExerciseUnit
        fields = ["id", "name", "unit"]


class ExerciseUnitInstanceSerializer(serializers.ModelSerializer):

    exercise_instance = serializers.HyperlinkedRelatedField(
        view_name="exerciseinstance-detail", read_only=True,
    )
    unit = ExerciseUnitSerializer(required=False, allow_null=True)
    unit_id = serializers.IntegerField(required=False)

    class Meta:
        model = ExerciseUnitInstance
        fields = ["id", "exercise_instance", "unit", "unit_id", "number"]


class ExerciseInstanceSerializer(serializers.HyperlinkedModelSerializer):
    workout = serializers.HyperlinkedRelatedField(
        queryset=Workout.objects.all(), view_name="workout-detail", required=False,
    )
    unit_instances = ExerciseUnitInstanceSerializer(many=True)

    class Meta:
        model = ExerciseInstance
        fields = ["url", "id", "exercise", "sets", "workout", "unit_instances"]

    def create(self, workout, validated_data):
        unit_instances_data = validated_data.pop("unit_instances")

        # Create ExerciseInstance object
        exercise_instance = ExerciseInstance.objects.create(
            workout=workout,
            **validated_data,
        )

        # Create ExerciseUnitInstance objects
        for unit_instance_data in unit_instances_data:
            unit_id = unit_instance_data["unit_id"]
            number = unit_instance_data["number"]
            unit = ExerciseUnit.objects.get(id=unit_id, exercise=exercise_instance.exercise)
            ExerciseUnitInstance.objects.create(exercise_instance=exercise_instance, unit=unit, number=number)
        return exercise_instance

    def update(self, instance, validated_data):
        """Update method.

        Not needed because the user should not be able to update their exercise instance.
        """


class WorkoutFileSerializer(serializers.HyperlinkedModelSerializer):
    """Serializer for a WorkoutFile. Hyperlinks are used for relationships by default.

    Serialized fields: url, id, owner, file, workout

    Attributes:
        owner:      The owner (User) of the WorkoutFile, represented by a username. ReadOnly
        workout:    The associate workout for this WorkoutFile, represented by a hyperlink
    """

    owner = serializers.ReadOnlyField(source="owner.username")
    workout = serializers.HyperlinkedRelatedField(
        queryset=Workout.objects.all(), view_name="workout-detail", required=False,
    )

    class Meta:
        model = WorkoutFile
        fields = ["url", "id", "owner", "file", "workout"]

    def create(self, validated_data):
        return WorkoutFile.objects.create(**validated_data)


class WorkoutSerializer(serializers.HyperlinkedModelSerializer):
    """Serializer for a Workout. Hyperlinks are used for relationships by default.

    This serializer specifies nested serialization since a workout consists of WorkoutFiles
    and ExerciseInstances.

    Serialized fields: url, id, name, date, notes, owner, owner_username, visiblity,
                       exercise_instances, files

    Attributes:
        owner_username:     Username of the owning User
        exercise_instance:  Serializer for ExericseInstances
        files:              Serializer for WorkoutFiles
    """

    owner_username = serializers.SerializerMethodField()
    exercise_instances = ExerciseInstanceSerializer(many=True, required=True)
    files = WorkoutFileSerializer(many=True, required=False)

    class Meta:
        model = Workout
        fields = [
            "url",
            "id",
            "name",
            "date",
            "notes",
            "owner",
            "owner_username",
            "visibility",
            "exercise_instances",
            "files",
        ]
        extra_kwargs = {"owner": {"read_only": True}}

    def create(self, validated_data):
        """Custom logic for creating ExerciseInstances, WorkoutFiles, and a Workout.

        This is needed to iterate over the files and exercise instances, since this serializer is
        nested.

        Args:
            validated_data: Validated files and exercise_instances

        Returns:
            Workout: A newly created Workout
        """
        exercise_instances_data = validated_data.pop("exercise_instances")
        files_data = []
        if "files" in validated_data:
            files_data = validated_data.pop("files")

        workout = Workout.objects.create(**validated_data)

        for exercise_instance_data in exercise_instances_data:
            serializer = ExerciseInstanceSerializer()
            serializer.create(workout=workout, validated_data=exercise_instance_data)
        for file_data in files_data:
            WorkoutFile.objects.create(
                workout=workout, owner=workout.owner, file=file_data.get("file"),
            )

        return workout

    def update(self, instance, validated_data):
        """Custom logic for updating a Workout with its ExerciseInstances and Workouts.

        This is needed because each object in both exercise_instances and files must be iterated
        over and handled individually.

        Args:
            instance (Workout): Current Workout object
            validated_data: Contains data for validated fields

        Returns:
            Workout: Updated Workout instance
        """
        exercise_instances_data = validated_data.pop("exercise_instances")
        exercise_instances = instance.exercise_instances

        instance.name = validated_data.get("name", instance.name)
        instance.notes = validated_data.get("notes", instance.notes)
        instance.visibility = validated_data.get("visibility", instance.visibility)
        instance.date = validated_data.get("date", instance.date)
        instance.save()

        # Handle ExerciseInstances

        # Delete all previous instances
        exercise_instances.all().delete()

        # (Re)create new instances
        for exercise_instance_data in exercise_instances_data:
            serializer = ExerciseInstanceSerializer()
            serializer.create(workout=instance, validated_data=exercise_instance_data)

        # Handle WorkoutFiles
        if "files" in validated_data:
            files_data = validated_data.pop("files")
            files = instance.files

            for file, file_data in zip(files.all(), files_data):
                file.file = file_data.get("file", file.file)

            # If new files have been added, creating new WorkoutFiles
            if len(files_data) > len(files.all()):
                for i in range(len(files.all()), len(files_data)):
                    WorkoutFile.objects.create(
                        workout=instance,
                        owner=instance.owner,
                        file=files_data[i].get("file"),
                    )
            # Else if files have been removed, delete WorkoutFiles
            elif len(files_data) < len(files.all()):
                for i in range(len(files_data), len(files.all())):
                    files.all()[i].delete()

        return instance

    def get_owner_username(self, obj):
        """Returns the owning user's username.

        Args:
            obj (Workout): Current Workout

        Returns:
            str: Username of owner
        """
        return obj.owner.username


class ExerciseSerializer(serializers.HyperlinkedModelSerializer):
    """Serializer for an Exercise. Hyperlinks are used for relationships by default.

    Serialized fields: url, id, name, description, units, instances

    Attributes:
        units:      Associated unit instances for this Exercise type.
        instances:  Associated exercise instances with this Exercise type. Hyperlinks.
    """

    units = ExerciseUnitSerializer(many=True)
    instances = serializers.HyperlinkedRelatedField(
        many=True, view_name="exerciseinstance-detail", read_only=True,
    )

    class Meta:
        model = Exercise
        fields = ["url", "id", "name", "description", "units", "instances"]

    def create(self, validated_data):
        units_data = validated_data.pop("units")
        exercise = Exercise.objects.create(**validated_data)
        for unit_data in units_data:
            ExerciseUnit.objects.create(exercise=exercise, **unit_data)
        return exercise


class RememberMeSerializer(serializers.HyperlinkedModelSerializer):
    """Serializer for an RememberMe. Hyperlinks are used for relationships by default.

    Serialized fields: remember_me

    Attributes:
        remember_me:    Value of cookie used for remember me functionality
    """

    class Meta:
        model = RememberMe
        fields = ["remember_me"]
