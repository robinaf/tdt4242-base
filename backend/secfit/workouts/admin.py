"""Module for registering models from workouts app to admin page so that they appear."""
from django.contrib import admin

# Register your models here.
from workouts.models import (
    Exercise,
    ExerciseInstance,
    ExerciseUnit,
    ExerciseUnitInstance,
    Workout,
    WorkoutFile,
)

admin.site.register(Exercise)
admin.site.register(ExerciseInstance)
admin.site.register(ExerciseUnit)
admin.site.register(ExerciseUnitInstance)
admin.site.register(Workout)
admin.site.register(WorkoutFile)
