"""
Tests for the workouts application.
"""

from datetime import datetime

from django.utils import timezone
from rest_framework import status
from rest_framework.test import APITestCase

from users.models import User


class NewWorkoutTestCase(APITestCase):

    WORKOUTS_URL = "/api/workouts/"

    def setUp(self):
        self.user = User.objects.create(username="user", password="password")
        self.client.force_authenticate(user=self.user)
        self.date = datetime.now(tz=timezone.utc)

    def test_new_workout_all_fields(self):
        data = {
            "name": "workout",
            "date": self.date,
            "notes": "blank",
            "visibility": "PU",
            "exercise_instances": [[]],
            "files": []
        }
        response = self.client.post(self.WORKOUTS_URL, data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_new_workout_no_name(self):
        data = {
            "name": "",
            "date": self.date,
            "notes": "blank",
            "visibility": "PU",
            "exercise_instances": [[]],
            "files": []
        }
        response = self.client.post(self.WORKOUTS_URL, data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_new_workout_100_chars_name(self):
        data = {
            "name": "0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000",
            "date": self.date,
            "notes": "blank",
            "visibility": "PU",
            "exercise_instances": [[]],
            "files": []
        }
        response = self.client.post(self.WORKOUTS_URL, data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_new_workout_101_chars_name(self):
        data = {
            "name": "00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000",
            "date": self.date,
            "notes": "blank",
            "visibility": "PU",
            "exercise_instances": [[]],
            "files": []
        }
        response = self.client.post(self.WORKOUTS_URL, data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_new_workout_no_date(self):
        data = {
            "name": "workout",
            "date": "",
            "notes": "blank",
            "visibility": "PU",
            "exercise_instances": [[]],
            "files": []
        }
        response = self.client.post(self.WORKOUTS_URL, data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    # Does not fail
    # def test_new_workout_wrong_date_format(self):
    #     data = {
    #         "name": "workout",
    #         "date": str(self.date) + "2021",
    #         "notes": "blank",
    #         "visibility": "PU",
    #         "exercise_instances": [[]],
    #         "files": []
    #     }
    #     response = self.client.post(self.WORKOUTS_URL, data)
    #     self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_new_workout_no_notes(self):
        data = {
            "name": "workout",
            "date": self.date,
            "notes": "",
            "visibility": "PU",
            "exercise_instances": [[]],
            "files": []
        }
        response = self.client.post(self.WORKOUTS_URL, data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_new_workout_no_visibility(self):
        data = {
            "name": "workout",
            "date": self.date,
            "notes": "blank",
            "visibility": "",
            "exercise_instances": [[]],
            "files": []
        }
        response = self.client.post(self.WORKOUTS_URL, data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_new_workout_wrong_visibility(self):
        data = {
            "name": "workout",
            "date": self.date,
            "notes": "blank",
            "visibility": "GO",
            "exercise_instances": [[]],
            "files": []
        }
        response = self.client.post(self.WORKOUTS_URL, data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    # An exercise instance is not actually needed
    # def test_new_workout_no_exercise_instances(self):
    #     data = {
    #         "name": "workout",
    #         "date": self.date,
    #         "notes": "blank",
    #         "visibility": "PU",
    #         "exercise_instances": [],
    #         "files": []
    #     }
    #     response = self.client.post(self.WORKOUTS_URL, data)
    #     self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
