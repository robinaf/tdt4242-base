from datetime import datetime
from unittest import TestCase

from django.utils import timezone
from mock import MagicMock

from users.models import User
from workouts.models import Workout
from workouts.permissions import (
    IsCoachAndVisibleToCoach,
    IsCoachOfWorkoutAndVisibleToCoach,
    IsOwner,
    IsOwnerOfWorkout,
    IsPublic,
    IsReadOnly,
    IsWorkoutPublic,
)


class IsOwnerTestCase(TestCase):

    def setUp(self):
        # Permission to test
        self.isOwner = IsOwner()

        # Create users
        self.user1 = "user1"
        self.user2 = "user2"

        # Set up request, view and object
        self.request = MagicMock(user=MagicMock(), data=MagicMock())
        self.view = MagicMock()
        self.obj = MagicMock(owner=MagicMock())

    def set_owner_and_user(self, owner, user):
        self.obj.owner = owner
        self.request.user = user

    def test_is_owner(self):
        self.set_owner_and_user(self.user1, self.user1)
        self.assertTrue(self.isOwner.has_object_permission(self.request, self.view, self.obj))

    def test_is_not_owner(self):
        self.set_owner_and_user(self.user1, self.user2)
        self.assertFalse(self.isOwner.has_object_permission(self.request, self.view, self.obj))


class IsOwnerOfWorkoutTestCase(TestCase):

    workout_data = {
        "name": "First workout",
        "owner": "",
        "date": datetime.now(tz=timezone.utc),
        "visibility": "PU"
    }

    def setUp(self):
        # Permission to test
        self.isOwnerOfWorkout = IsOwnerOfWorkout()

        # Create users
        self.user = "user"

        # Set up request, view and object
        self.request = MagicMock(data=MagicMock())
        self.view = MagicMock()
        self.obj = MagicMock(workout=MagicMock())
        User.objects.all().delete()

    def create_workout_and_request(self, username, id):
        user_obj = User(username=username)
        user_obj.save()
        self.request.user = user_obj
        self.request.method = "POST"
        self.request.data = {"workout": "/" + id + "/First workout"}
        self.workout_data["owner"] = self.request.user
        Workout.objects.create(**self.workout_data)

    def test_is_owner_of_workout_post(self):
        self.create_workout_and_request("user", "2")
        self.assertTrue(self.isOwnerOfWorkout.has_permission(self.request, self.view))

    def test_is_not_owner_of_workout_post(self):
        self.create_workout_and_request("user", "1")
        self.request.user = "not_user"
        self.assertFalse(self.isOwnerOfWorkout.has_permission(self.request, self.view))

    def test_is_owner_of_workout_get(self):
        self.request.method = "GET"
        self.assertTrue(self.isOwnerOfWorkout.has_permission(self.request, self.view))

    def test_is_owner_of_workout_no_data(self):
        self.request.method = "POST"
        self.request.data = {"not": "workout"}
        self.assertFalse(self.isOwnerOfWorkout.has_permission(self.request, self.view))

    def test_is_owner_of_workout_object(self):
        self.request.user = self.user
        self.obj.workout.owner = self.user
        self.assertTrue(self.isOwnerOfWorkout.has_object_permission(self.request, self.view, self.obj))


class IsCoachAndVisibleToCoachTestCase(TestCase):

    def setUp(self):
        # Permission to test
        self.isCoachAndVisibleToCoach = IsCoachAndVisibleToCoach()

        # Create users
        self.coach = "coach"

        # Set up request, view and object
        self.request = MagicMock()
        self.view = MagicMock()
        self.obj = MagicMock(owner=MagicMock())

    def set_coach_user_visibility(self, coach, user, visibility):
        self.obj.owner.coach = coach
        self.request.user = user
        self.obj.visibility = visibility

    def test_is_coach_and_visible_to_coach(self):
        self.set_coach_user_visibility(self.coach, self.coach, "CO")
        self.assertTrue(self.isCoachAndVisibleToCoach.has_object_permission(self.request, self.view, self.obj))

    def test_is_coach_and_visible_to_coach_public(self):
        self.set_coach_user_visibility(self.coach, self.coach, "PU")
        self.assertTrue(self.isCoachAndVisibleToCoach.has_object_permission(self.request, self.view, self.obj))

    def test_is_coach_and_not_visible_to_coach_private(self):
        self.set_coach_user_visibility(self.coach, self.coach, "PR")
        self.assertFalse(self.isCoachAndVisibleToCoach.has_object_permission(self.request, self.view, self.obj))


class IsCoachOfWorkoutAndVisibleToCoachTestCase(TestCase):

    def setUp(self):
        # Permission to test
        self.isCoachOfWorkoutAndVisibleToCoach = IsCoachOfWorkoutAndVisibleToCoach()

        # Create users
        self.coach = "coach"

        # Set up request, view and object
        self.request = MagicMock()
        self.view = MagicMock()
        self.obj = MagicMock(workout=MagicMock(owner=MagicMock()))

    def set_coach_user_visibility(self, coach, user, visibility):
        self.obj.workout.owner.coach = coach
        self.request.user = user
        self.obj.workout.visibility = visibility

    def test_is_coach_of_workout_and_visible_to_coach(self):
        self.set_coach_user_visibility(self.coach, self.coach, "CO")
        self.assertTrue(self.isCoachOfWorkoutAndVisibleToCoach.has_object_permission(self.request, self.view, self.obj))

    def test_is_coach_of_workout_and_visible_to_coach_public(self):
        self.set_coach_user_visibility(self.coach, self.coach, "PU")
        self.assertTrue(self.isCoachOfWorkoutAndVisibleToCoach.has_object_permission(self.request, self.view, self.obj))

    def test_is_coach_of_workout_and_not_visible_to_coach_private(self):
        self.set_coach_user_visibility(self.coach, self.coach, "PR")
        self.assertFalse(self.isCoachOfWorkoutAndVisibleToCoach.has_object_permission(self.request, self.view, self.obj))


class IsPublicTestCase(TestCase):

    def setUp(self):
        # Permission to test
        self.isPublic = IsPublic()

        # Set up request, view and object
        self.request = MagicMock()
        self.view = MagicMock()
        self.obj = MagicMock()

    def set_visibility(self, visibility):
        self.obj.visibility = visibility

    def test_is_public(self):
        self.set_visibility("PU")
        self.assertTrue(self.isPublic.has_object_permission(self.request, self.view, self.obj))

    def test_is_not_public(self):
        self.set_visibility("PR")
        self.assertFalse(self.isPublic.has_object_permission(self.request, self.view, self.obj))


class IsWorkoutPublicTestCase(TestCase):

    def setUp(self):
        # Permission to test
        self.isWorkoutPublic = IsWorkoutPublic()

        # Set up request, view and object
        self.request = MagicMock()
        self.view = MagicMock()
        self.obj = MagicMock(workout=MagicMock())

    def set_visibility(self, visibility):
        self.obj.workout.visibility = visibility

    def test_is_workout_public(self):
        self.set_visibility("PU")
        self.assertTrue(self.isWorkoutPublic.has_object_permission(self.request, self.view, self.obj))

    def test_is_workout_not_public(self):
        self.set_visibility("PR")
        self.assertFalse(self.isWorkoutPublic.has_object_permission(self.request, self.view, self.obj))


class IsReadOnlyTestCase(TestCase):

    def setUp(self):
        # Permission to test
        self.isReadOnly = IsReadOnly()

        # Set up request, view and object
        self.request = MagicMock()
        self.view = MagicMock()
        self.obj = MagicMock()

    def set_method(self, method):
        self.request.method = method

    def test_is_read_only_get(self):
        self.set_method("GET")
        self.assertTrue(self.isReadOnly.has_object_permission(self.request, self.view, self.obj))

    def test_is_read_only_head(self):
        self.set_method("HEAD")
        self.assertTrue(self.isReadOnly.has_object_permission(self.request, self.view, self.obj))

    def test_is_read_only_options(self):
        self.set_method("OPTIONS")
        self.assertTrue(self.isReadOnly.has_object_permission(self.request, self.view, self.obj))

    def test_is_read_only_post(self):
        self.set_method("POST")
        self.assertFalse(self.isReadOnly.has_object_permission(self.request, self.view, self.obj))
