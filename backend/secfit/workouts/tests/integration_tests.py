import copy

from rest_framework import status
from rest_framework.test import APITestCase

from users.models import User
from workouts.models import Exercise, Workout
from workouts.serializers import ExerciseSerializer, WorkoutSerializer


class IntegrationTestCase(APITestCase):
    exercise_glob = None

    def setUp(self):
        self.user = User.objects.create(username="user", password="password")
        self.client.force_authenticate(user=self.user)

    def test_create_new_exercise(self):

        data = {
            'name': "Bench",
            'description': "desc",
            'units': [{'name': "repetitions", 'unit': "reps"}]
            }

        data_copy = copy.deepcopy(data)
        serializer = ExerciseSerializer()
        exercise = serializer.create(data_copy)
        self.exercise_glob = copy.deepcopy(exercise)
        exercise_obj = Exercise.objects.get(id=exercise.id)
        response = self.client.get("/api/exercises/" + str(exercise.id) + "/")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(exercise, exercise_obj)
        self.assertEqual(exercise_obj.name, data['name'])
        self.assertEqual(exercise_obj.description, data['description'])
        self.assertEqual(exercise_obj.units.all()[0].name, data['units'][0]['name'])
        self.assertEqual(exercise_obj.units.all()[0].unit, data['units'][0]['unit'])

    def test_create_new_workout(self):
        # Create new exercise
        data = {
            'name': "Bench",
            'description': "desc",
            'units': [{'name': "repetitions", 'unit': "reps"}]
            }

        data_copy = copy.deepcopy(data)
        serializer = ExerciseSerializer()
        exercise = serializer.create(data_copy)
        unit = exercise.units.all()[0]
        exercise_instance = {
            "sets": 3,
            "exercise_id": exercise.id,
            "unit_instances": [{'unit': unit, 'unit_id': unit.id, 'number': 3}]
        }
        exercise_instance_copy = copy.deepcopy(exercise_instance)
        data = {
            "owner_id": self.user.id,
            "name": "workout",
            "date": "2021-03-20T10:00",
            "notes": "note",
            "visibility": "PU",
            "exercise_instances": [exercise_instance_copy],
            "files": []
        }

        # Create new workout
        serializer = WorkoutSerializer()
        workout = serializer.create(data)
        workout_obj = Workout.objects.get(id=workout.id)
        response = self.client.get("/api/workouts/" + str(workout.id) + "/")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(workout, workout_obj)
        self.assertEqual(workout_obj.name, data['name'])
        exercise_instance_obj = workout_obj.exercise_instances.all()[0]
        unit_instance_obj = exercise_instance_obj.unit_instances.all()[0]
        self.assertEqual(unit_instance_obj.number, exercise_instance['unit_instances'][0]['number'])
        self.assertEqual(unit_instance_obj.id, exercise_instance['unit_instances'][0]['unit_id'])
        self.assertEqual(unit_instance_obj.unit, unit)
