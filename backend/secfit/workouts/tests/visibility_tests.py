from datetime import datetime

from django.utils import timezone
from rest_framework import status
from rest_framework.test import APITestCase

from comments.models import Comment
from users.models import User
from workouts.models import Workout, WorkoutFile


class AthleteCanSeeWorkoutTestCase(APITestCase):

    WORKOUTS_URL = "/api/workouts/"
    WORKOUT_FILES_URL = "/api/workout-files/"
    COMMENTS_URL = "/api/comments/"

    def setUp(self):
        self.user = User.objects.create(username="user", password="password")
        self.client.force_authenticate(user=self.user)

    def createWorkout(self, visiblity, owner):
        date = datetime.now(tz=timezone.utc)
        return Workout.objects.create(name="workout", date=date, notes="note", visibility=visiblity, owner=owner)

    def createFile(self, workout, owner):
        return WorkoutFile.objects.create(workout=workout, owner=owner)

    def createComment(self, workout, owner):
        return Comment.objects.create(workout=workout, owner=owner)

    def test_athlete_can_see_private_workout(self):
        workout = self.createWorkout("PR", self.user)
        response = self.client.get(self.WORKOUTS_URL + str(workout.id) + "/")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        file = self.createFile(workout, self.user)
        response = self.client.get(self.WORKOUT_FILES_URL + str(file.id) + "/")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        comment = self.createComment(workout, self.user)
        response = self.client.get(self.COMMENTS_URL + str(comment.id) + "/")
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_athlete_can_see_public_workout(self):
        workout = self.createWorkout("PU", self.user)
        response = self.client.get(self.WORKOUTS_URL + str(workout.id) + "/")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        file = self.createFile(workout, self.user)
        response = self.client.get(self.WORKOUT_FILES_URL + str(file.id) + "/")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        comment = self.createComment(workout, self.user)
        response = self.client.get(self.COMMENTS_URL + str(comment.id) + "/")
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_athlete_can_see_public_workout_other_user(self):
        other_user = User.objects.create(username="other_user", password="password")
        workout = self.createWorkout("PU", other_user)
        response = self.client.get(self.WORKOUTS_URL + str(workout.id) + "/")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        file = self.createFile(workout, self.user)
        response = self.client.get(self.WORKOUT_FILES_URL + str(file.id) + "/")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        comment = self.createComment(workout, self.user)
        response = self.client.get(self.COMMENTS_URL + str(comment.id) + "/")
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_athlete_cannot_see_private_workout_other_user(self):
        other_user = User.objects.create(username="other_user", password="password")
        workout = self.createWorkout("PR", other_user)
        response = self.client.get(self.WORKOUTS_URL + str(workout.id) + "/")
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        file = self.createFile(workout, other_user)
        response = self.client.get(self.WORKOUT_FILES_URL + str(file.id) + "/")
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        comment = self.createComment(workout, other_user)
        response = self.client.get(self.COMMENTS_URL + str(comment.id) + "/")
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_athlete_cannot_see_coach_workout_other_user(self):
        other_user = User.objects.create(username="other_user", password="password")
        workout = self.createWorkout("CO", other_user)
        response = self.client.get(self.WORKOUTS_URL + str(workout.id) + "/")
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        file = self.createFile(workout, other_user)
        response = self.client.get(self.WORKOUT_FILES_URL + str(file.id) + "/")
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        comment = self.createComment(workout, other_user)
        response = self.client.get(self.COMMENTS_URL + str(comment.id) + "/")
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)


class CoachCanSeeWorkoutTestCase(APITestCase):

    WORKOUTS_URL = "/api/workouts/"
    WORKOUT_FILES_URL = "/api/workout-files/"
    COMMENTS_URL = "/api/comments/"

    def setUp(self):
        self.coach = User.objects.create(username="coach", password="password")
        self.user = User.objects.create(username="user", password="password", coach=self.coach)
        self.client.force_authenticate(user=self.coach)

    def createWorkout(self, visiblity, owner):
        date = datetime.now(tz=timezone.utc)
        return Workout.objects.create(name="workout", date=date, notes="note", visibility=visiblity, owner=owner)

    def createFile(self, workout, owner):
        return WorkoutFile.objects.create(workout=workout, owner=owner)

    def createComment(self, workout, owner):
        return Comment.objects.create(workout=workout, owner=owner)

    def test_coach_can_see_public_workout(self):
        workout = self.createWorkout("PU", self.user)
        response = self.client.get(self.WORKOUTS_URL + str(workout.id) + "/")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        file = self.createFile(workout, self.user)
        response = self.client.get(self.WORKOUT_FILES_URL + str(file.id) + "/")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        comment = self.createComment(workout, self.user)
        response = self.client.get(self.COMMENTS_URL + str(comment.id) + "/")
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_coach_can_see_public_workout_other_user(self):
        other_user = User.objects.create(username="other_user", password="password")
        workout = self.createWorkout("PU", other_user)
        response = self.client.get(self.WORKOUTS_URL + str(workout.id) + "/")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        file = self.createFile(workout, other_user)
        response = self.client.get(self.WORKOUT_FILES_URL + str(file.id) + "/")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        comment = self.createComment(workout, other_user)
        response = self.client.get(self.COMMENTS_URL + str(comment.id) + "/")
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_coach_can_see_coach_workout(self):
        workout = self.createWorkout("CO", self.user)
        response = self.client.get(self.WORKOUTS_URL + str(workout.id) + "/")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        file = self.createFile(workout, self.user)
        response = self.client.get(self.WORKOUT_FILES_URL + str(file.id) + "/")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        comment = self.createComment(workout, self.user)
        response = self.client.get(self.COMMENTS_URL + str(comment.id) + "/")
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_coach_cannot_see_coach_workout_other_user(self):
        other_user = User.objects.create(username="other_user", password="password")
        workout = self.createWorkout("CO", other_user)
        response = self.client.get(self.WORKOUTS_URL + str(workout.id) + "/")
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        file = self.createFile(workout, other_user)
        response = self.client.get(self.WORKOUT_FILES_URL + str(file.id) + "/")
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        comment = self.createComment(workout, other_user)
        response = self.client.get(self.COMMENTS_URL + str(comment.id) + "/")
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_coach_cannot_see_private_workout(self):
        workout = self.createWorkout("PR", self.user)
        response = self.client.get(self.WORKOUTS_URL + str(workout.id) + "/")
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        file = self.createFile(workout, self.user)
        response = self.client.get(self.WORKOUT_FILES_URL + str(file.id) + "/")
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        comment = self.createComment(workout, self.user)
        response = self.client.get(self.COMMENTS_URL + str(comment.id) + "/")
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_coach_cannot_see_private_workout_other_user(self):
        other_user = User.objects.create(username="other_user", password="password")
        workout = self.createWorkout("PR", other_user)
        response = self.client.get(self.WORKOUTS_URL + str(workout.id) + "/")
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        file = self.createFile(workout, other_user)
        response = self.client.get(self.WORKOUT_FILES_URL + str(file.id) + "/")
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        comment = self.createComment(workout, other_user)
        response = self.client.get(self.COMMENTS_URL + str(comment.id) + "/")
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
