from allpairspy import AllPairs
from django.test import TestCase
from rest_framework import status
from rest_framework.test import APITestCase

from users.serializers import UserSerializer

USERS_URL = "/api/users/"


class RegistrationTestCase(APITestCase):

    def test_registration_all_fields(self):
        data = {
            "username": "user",
            "email": "something@mail.com",
            "password": "password",
            "password1": "password",
            "phone_number": "22225555",
            "country": "Norway",
            "city": "Trondheim",
            "street_address": "Torget",
        }
        response = self.client.post(USERS_URL, data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_registration_minimal_information(self):
        data = {
            "username": "user",
            "email": "",
            "password": "password",
            "password1": "password",
            "phone_number": "",
            "country": "",
            "city": "",
            "street_address": "",
        }
        response = self.client.post(USERS_URL, data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_registration_no_username(self):
        data = {
            "username": "",
            "email": "",
            "password": "password",
            "password1": "password",
            "phone_number": "",
            "country": "",
            "city": "",
            "street_address": "",
        }
        response = self.client.post(USERS_URL, data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_registration_no_password(self):
        data = {
            "username": "user",
            "email": "",
            "password": "",
            "password1": "password",
            "phone_number": "",
            "country": "",
            "city": "",
            "street_address": "",
        }
        response = self.client.post(USERS_URL, data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_registration_no_password1(self):
        data = {
            "username": "user",
            "email": "",
            "password": "password",
            "password1": "",
            "phone_number": "",
            "country": "",
            "city": "",
            "street_address": "",
        }
        response = self.client.post(USERS_URL, data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_registration_wrong_email_format(self):
        data = {
            "username": "user",
            "email": "bad_email.com",
            "password": "password",
            "password1": "password",
            "phone_number": "",
            "country": "",
            "city": "",
            "street_address": "",
        }
        response = self.client.post(USERS_URL, data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_registration_12_chars_phone_number(self):
        data = {
            "username": "user",
            "email": "",
            "password": "password",
            "password1": "password",
            "phone_number": "004798765432",
            "country": "",
            "city": "",
            "street_address": "",
        }
        response = self.client.post(USERS_URL, data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_registration_13_chars_phone_number(self):
        data = {
            "username": "user",
            "email": "",
            "password": "password",
            "password1": "password",
            "phone_number": "0047987654321",
            "country": "",
            "city": "",
            "street_address": "",
        }
        response = self.client.post(USERS_URL, data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_registration_50_chars_country(self):
        data = {
            "username": "user",
            "email": "",
            "password": "password",
            "password1": "password",
            "phone_number": "",
            "country": "00000000000000000000000000000000000000000000000000",
            "city": "",
            "street_address": "",
        }
        response = self.client.post(USERS_URL, data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_registration_51_chars_country(self):
        data = {
            "username": "user",
            "email": "",
            "password": "password",
            "password1": "password",
            "phone_number": "",
            "country": "000000000000000000000000000000000000000000000000000",
            "city": "",
            "street_address": "",
        }
        response = self.client.post(USERS_URL, data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_registration_50_chars_city(self):
        data = {
            "username": "user",
            "email": "",
            "password": "password",
            "password1": "password",
            "phone_number": "",
            "country": "",
            "city": "00000000000000000000000000000000000000000000000000",
            "street_address": "",
        }
        response = self.client.post(USERS_URL, data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_registration_51_chars_city(self):
        data = {
            "username": "user",
            "email": "",
            "password": "password",
            "password1": "password",
            "phone_number": "",
            "country": "",
            "city": "000000000000000000000000000000000000000000000000000",
            "street_address": "",
        }
        response = self.client.post(USERS_URL, data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_registration_50_chars_street_address(self):
        data = {
            "username": "user",
            "email": "",
            "password": "password",
            "password1": "password",
            "phone_number": "",
            "country": "",
            "city": "",
            "street_address": "00000000000000000000000000000000000000000000000000",
        }
        response = self.client.post(USERS_URL, data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_registration_51_chars_street_address(self):
        data = {
            "username": "user",
            "email": "",
            "password": "password",
            "password1": "password",
            "phone_number": "",
            "country": "",
            "city": "",
            "street_address": "000000000000000000000000000000000000000000000000000",
        }
        response = self.client.post(USERS_URL, data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)


class UserSerializerTestCase(TestCase):

    def setUp(self):
        self.serializer = UserSerializer()
        self.data = {
            "username": "user3",
            "email": "something@mail.com",
            "password": "password",
            "password1": "password",
            "phone_number": "22225555",
            "country": "Norway",
            "city": "Trondheim",
            "street_address": "Torget",
        }

    def test_user_serializer_create(self):
        user_obj = UserSerializer.create(self.serializer, self.data)
        self.assertEqual(user_obj.username, self.data["username"])
        self.assertEqual(user_obj.email, self.data["email"])
        self.assertEqual(user_obj.phone_number, self.data["phone_number"])
        self.assertEqual(user_obj.country, self.data["country"])
        self.assertEqual(user_obj.city, self.data["city"])
        self.assertEqual(user_obj.street_address, self.data["street_address"])

    def test_user_serializer_too_short_password(self):
        data = {
            "username": "user",
            "email": "",
            "password": "pass",
            "password1": "pass",
            "phone_number": "",
            "country": "",
            "city": "",
            "street_address": "",
        }
        response = self.client.post(USERS_URL, data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_user_serializer_passwords_no_match(self):
        data = {
            "username": "user",
            "email": "",
            "password": "password",
            "password1": "not_same_password",
            "phone_number": "",
            "country": "",
            "city": "",
            "street_address": "",
        }
        response = self.client.post(USERS_URL, data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)


class Registration2WayDomainTestCase(APITestCase):

    def test_registration_domain(self):

        bad_email = "bad_email.com"
        unsupported_chars = "*%#?+"

        parameters = [
            ["user", unsupported_chars, ""],
            ["email@mail.com", bad_email, ""],
            ["password", ""],
            ["password", ""],
            ["22225555", ""],
            ["Norway", ""],
            ["Trondheim", ""],
            ["Torget", ""],
        ]

        data = {
            "username": "",
            "email": "",
            "password": "",
            "password1": "",
            "phone_number": "",
            "country": "",
            "city": "",
            "street_address": "",
        }

        for pairs in AllPairs(parameters):
            code = status.HTTP_201_CREATED

            for j, key in enumerate(data):
                data[key] = pairs[j]

            if data["username"] == "" or data["username"] == unsupported_chars or data["email"] == bad_email or data["password"] == "" or data["password1"] == "":
                code = status.HTTP_400_BAD_REQUEST

            response = self.client.post(USERS_URL, data)
            self.assertEqual(response.status_code, code)
