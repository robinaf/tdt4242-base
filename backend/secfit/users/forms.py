from django import forms
from django.contrib.auth import get_user_model
from django.contrib.auth.forms import UserChangeForm, UserCreationForm


class CustomUserCreationForm(UserCreationForm):
    PHONE_NUMBER_LENGTH = 12
    LOCATION_STRING_LENGTH = 50

    phone_number = forms.CharField(max_length=PHONE_NUMBER_LENGTH)
    country = forms.CharField(max_length=LOCATION_STRING_LENGTH)
    city = forms.CharField(max_length=LOCATION_STRING_LENGTH)
    street_address = forms.CharField(max_length=LOCATION_STRING_LENGTH)

    class Meta(UserCreationForm):
        model = get_user_model()
        fields = ("username", "coach", "phone_number", "country", "city", "street_address")


class CustomUserChangeForm(UserChangeForm):
    class Meta:
        model = get_user_model()
        fields = ("username", "coach")
