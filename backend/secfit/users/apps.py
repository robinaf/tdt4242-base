from django.apps import AppConfig


class UsersConfig(AppConfig):
    """AppConfig for users app.

    Attributes:
        name (str): The name of the application
    """

    name = "users"
