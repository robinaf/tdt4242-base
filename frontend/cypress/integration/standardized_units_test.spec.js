describe('test exercise with new units', () => {
  const name = "Push ups3"
  const desc = "Chest exercise"
  const unitName1 = "repetitions"
  const unitName2 = "weight"
  const unitType1 = "reps"
  const unitType2 = "kg"

  beforeEach(() => {
    cy.login('admin', 'admin')
  });

  //Creates a new exercise with two different units
  it('add new exercise', () => {
    cy.visit('http://localhost:3000/exercises.html')
    cy.get('#btn-create-exercise').click();

    cy.get('#inputName').type(name)
    cy.get('#inputDescription').type(desc)
    cy.get('#btn-add-unit').click();
    cy.get('#btn-add-unit').click();
    cy.get('[name="0:name"]').type(unitName1)
    cy.get('[name="1:name"]').type(unitName2)
    cy.get('[id=select-1]').select(unitType2)
    cy.get('#btn-ok-exercise').click()

  });

  //Checks if the new exercise was created with the correct values
  it('see new exercise', () => {
    cy.visit('http://localhost:3000/exercises.html')
    cy.wait(1000)
    cy.get('#div-content.list-group.mt-1').children().last().click({force: true})
    cy.get('#inputName').should('have.value', name)
    cy.get('#inputDescription').should('have.value', desc)
    cy.get('[name="0:name"]').should('have.value', unitName1)
    cy.get('[name="1:name"]').should('have.value', unitName2)
    cy.get('[id=select-0]').should('have.value', unitType1)
    cy.get('[id=select-1]').should('have.value', unitType2)

  });

  //Creates a workout using the newly created exercise
  it('create workout with new exercise', () => {
    cy.visit('http://localhost:3000/workouts.html')
    cy.get('#btn-create-workout').click()
    cy.wait(2000)
    cy.get('#inputName').type('Workout')
    cy.get('#inputDateTime').type('2021-03-20T10:00')
    cy.get('#inputNotes').type('note')
    cy.get('select[name="type"]').select(name, {force: true})
    cy.get('input[name="sets"]').type(3, {force: true})
    cy.get('#div-exercises').contains(unitName1).next().type(10, {force: true})
    cy.get('#div-exercises').contains(unitName2).next().type(5, {force: true})
    cy.get('#btn-ok-workout').click()


  });

  //Checks if the new workout was created with the correct values
  it('see workout with new exercise', () => {
    cy.visit('http://localhost:3000/workouts.html')
    cy.wait(2000)
    cy.get('#div-content').children().last().click({force: true})
    cy.get('#inputName').should('have.value', 'Workout')
    cy.get('#inputDateTime').should('have.value', '2021-03-20T10:00:00')
    cy.get('#inputNotes').should('have.value', 'note')
    cy.get('select[name="type"]').find('option:selected').should('have.text', name, {force: true})
    cy.get('input[name="sets"]').should('have.value', '3', {force: true})
    cy.get('#div-exercises').contains(unitName1).next().should('have.value', '10', {force: true})
    cy.get('#div-exercises').contains(unitName2).next().should('have.value', '5', {force: true})


  });

});
