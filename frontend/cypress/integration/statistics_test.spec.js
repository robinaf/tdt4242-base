describe('test statistics', () => {
  const name = "Push ups1"
  const unitName1 = "repetitions"
  const unitName2 = "weight"

  beforeEach(() => {
    cy.login('admin', 'admin')
  });


  it('see statistics of exercise', () => {
    cy.visit('http://localhost:3000/exercises.html')
    cy.wait(1000)
    cy.get('#div-content.list-group.mt-1').children().last().click({force: true})
    cy.get('#myChart').should('be.visible')

  });
});
