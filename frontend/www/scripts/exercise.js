let cancelButton;
let okButton;
let deleteButton;
let addUnitButton;

function populateForm(exerciseData) {
    const form = document.querySelector('#form-exercise');

    // Name
    const nameInput = form.querySelector(`input[name="name"]`);
    nameInput.value = exerciseData.name;

    // Description
    const descriptionInput = form.querySelector(`textarea[name="description"]`);
    descriptionInput.value = exerciseData.description;

    // Units
    const unitContainer = document.querySelector('#unitContainer');
    unitContainer.innerHTML = '';

    const unitOptions = ['reps', 'kg', 'km', 'min'];
    const unitsData = exerciseData.units;
    unitsData.forEach((unitData, index) => {
        const unitRowTemplate = document.querySelector('#template-unit-row');
        const unitRowContainer = unitRowTemplate.content.firstElementChild.cloneNode(true);

        // Hidden id field
        const hiddenIdField = unitRowContainer.querySelector('.hiddenIdField');
        hiddenIdField.name = `${index}:id`;

        // Name input
        const unitNameInput = unitRowContainer.querySelector('.unitNameInput');
        unitNameInput.name = `${index}:name`;
        unitNameInput.value = unitData.name;

        // Select input
        const selectList = unitRowContainer.querySelector('.unitSelect');
        selectList.id = `select-${index}`;

        for (let i = 0; i < unitOptions.length; i++) {
            const option = document.createElement('option');
            option.value = unitOptions[i];
            option.text = unitOptions[i];
            option.selected = option.value === unitData.unit;
            selectList.appendChild(option);
        }

        const removeButton = unitRowContainer.querySelector('.removeBtn');
        removeButton.onclick = () => removeButton.parentNode.parentNode.remove();

        // Add unitRowContainer to unitContainer
        unitContainer.appendChild(unitRowContainer);
    });
}

function validateExerciseData(exerciseData) {
    clearAlerts();

    const unitsData = exerciseData.units;

    const unitHasNoName = unitsData.some((unit) => unit.name === '');
    if (unitHasNoName) {
        const alert = createAlert("There's a unit with no name.", {});
        document.body.prepend(alert);
        return false;
    }

    const unitNames = unitsData.map((unit) => unit.name);
    for (const [index, unitName] of unitNames.entries()) {
        if (unitNames.indexOf(unitName) !== index) {
            const alert = createAlert(
                `There can only be one unit with the name '${unitName}'.`,
                {}
            );
            document.body.prepend(alert);
            return false;
        }
    }

    const unitNameIsOnlyNumeric = unitsData.some((unit) => !isNaN(unit.name));
    if (unitNameIsOnlyNumeric) {
        const alert = createAlert("Unit names can't be purely numeric.", {});
        document.body.prepend(alert);
        return false;
    }

    return true;
}

function handleAddUnitButtonClick() {
    const exerciseData = getFormData();
    exerciseData.units.push({ name: '', unit: 'reps' });
    populateForm(exerciseData);
}

function setElementVisibility(element, visible) {
    if (visible) {
        element.className = element.className.replaceAll('hide', '');
        element.className = element.className.replaceAll('  ', ' ');
    } else {
        element.className += ' hide';
    }
}

function setExerciseState(state) {
    const isReadState = state === 'read';
    const isCreateState = state === 'create';

    // Read state
    setReadOnly(isReadState, '#form-exercise');
    setElementVisibility(deleteButton, isReadState);
    const unitSelect = document.querySelectorAll('.unitSelect');
    unitSelect.forEach((select) => (select.disabled = isReadState));
    const chart = document.querySelector('#myChart');
    setElementVisibility(chart, isReadState);

    // Create state
    setElementVisibility(okButton, isCreateState);
    setElementVisibility(cancelButton, isCreateState);
    setElementVisibility(addUnitButton, isCreateState);
    const removeButtons = document.querySelectorAll('.removeBtn');
    removeButtons.forEach((btn) => setElementVisibility(btn, isCreateState));
}

function handleCancelButtonDuringCreate() {
    window.location.replace('exercises.html');
}

function getFormData() {
    const form = document.querySelector('#form-exercise');
    const formData = new FormData(form);

    const body = {
        name: formData.get('name'),
        description: formData.get('description'),
        units: {},
    };

    // Group unit data
    const groupedUnits = {};
    Array.from(formData)
        .slice(2)
        .forEach((el) => {
            const [name, value] = el;
            const [idNumber, key] = name.split(':');

            if (key === 'id') {
                groupedUnits[idNumber] = {};
            } else {
                groupedUnits[idNumber][key] = value;
            }
        });

    // Add selected unit to groupedUnits
    for (const idNumber of Object.keys(groupedUnits)) {
        const select = document.querySelector(`#select-${idNumber}`);
        const selectedUnit = select.options[select.selectedIndex].value;
        groupedUnits[idNumber].unit = selectedUnit;
    }

    body.units = Object.values(groupedUnits);
    return body;
}

async function createExercise() {
    const body = getFormData();

    // Validate data
    const validated = validateExerciseData(body);
    if (!validated) return;

    const response = await sendRequest('POST', `${HOST}/api/exercises/`, body);

    if (response.ok) {
        window.location.replace('exercises.html');
    } else {
        const data = await response.json();
        const alert = createAlert('Could not create new exercise!', data);
        document.body.prepend(alert);
    }
}

async function deleteExercise(id) {
    const response = await sendRequest('DELETE', `${HOST}/api/exercises/${id}/`);
    if (!response.ok) {
        const data = await response.json();
        const alert = createAlert(`Could not delete exercise ${id}`, data);
        document.body.prepend(alert);
    } else {
        window.location.replace('exercises.html');
    }
}

async function retrieveExercise(id) {
    const response = await sendRequest('GET', `${HOST}/api/exercises/${id}/`);
    if (!response.ok) {
        const data = await response.json();
        const alert = createAlert('Could not retrieve exercise data!', data);
        document.body.prepend(alert);
    } else {
        const exerciseData = await response.json();
        populateForm(exerciseData);
    }
}

async function fetchWorkoutsWithExercise(ordering, id) {
    const response = await sendRequest('GET', `${HOST}/api/workouts/?ordering=${ordering}`);
    const dates = [];
    const datasets = [];
    const dataDict = {};

    if (!response.ok) {
        throw new Error(`HTTP error! status: ${response.status}`);
    } else {
        const data = await response.json();
        const workouts = data.results;

        const currentUser = await getCurrentUser();
        const currentUserUrl = currentUser.url;
        const athletesUrls = currentUser.athletes;

        workouts.forEach((workout) => {
            workout.exercise_instances.forEach((exercise) => {
                const url = exercise.exercise;
                const exerciseId = url.charAt(url.length - 2);

                if (
                    exerciseId === id &&
                    [currentUserUrl, ...athletesUrls].includes(workout.owner)
                ) {
                    const date = new Date(workout.date);
                    dates.push(date);
                    const { sets } = exercise;
                    if ('sets' in dataDict) {
                        dataDict.sets.push(sets);
                    } else {
                        dataDict.sets = [sets];
                    }
                    exercise.unit_instances.forEach((unitInstance) => {
                        const key = unitInstance.unit.name;
                        if (key in dataDict) {
                            dataDict[key].push(unitInstance.number);
                        } else {
                            dataDict[key] = [unitInstance.number];
                        }
                    });
                }
            });
        });
        Object.entries(dataDict).forEach(([key, value]) => {
            const dataEntry = {
                data: value,
                label: key,
                borderColor: `#${((Math.random() * 0xffffff) << 0).toString(16)}`,
                fill: false,
            };
            datasets.push(dataEntry);
        });
        return [dates, datasets];
    }
}

async function populateExerciseGraph(ordering, id) {
    const values = await fetchWorkoutsWithExercise(ordering, id);
    const dates = values[0];
    const datasets = values[1];
    const myChart = document.getElementById('myChart');
    if (dates.length > 0) {
        setElementVisibility(myChart, true);
    } else {
        setElementVisibility(myChart, false);
    }
    new Chart(document.getElementById('myChart'), {
        type: 'line',
        data: {
            labels: dates,
            datasets,
        },
        options: {
            scales: {
                xAxes: [
                    {
                        type: 'time',
                        distribution: 'linear',
                    },
                ],
            },
        },
    });
}

window.addEventListener('DOMContentLoaded', async () => {
    cancelButton = document.querySelector('#btn-cancel-exercise');
    okButton = document.querySelector('#btn-ok-exercise');
    deleteButton = document.querySelector('#btn-delete-exercise');
    addUnitButton = document.querySelector('#btn-add-unit');
    const ordering = '-date';
    let exerciseId = null;

    const urlParams = new URLSearchParams(window.location.search);

    // View
    if (urlParams.has('id')) {
        exerciseId = urlParams.get('id');
        await retrieveExercise(exerciseId);

        deleteButton.addEventListener(
            'click',
            (async (id) => await deleteExercise(id)).bind(undefined, exerciseId)
        );
        setExerciseState('read');
    }
    // Create
    else {
        okButton.addEventListener('click', async () => await createExercise());
        cancelButton.addEventListener('click', handleCancelButtonDuringCreate);
        addUnitButton.addEventListener('click', handleAddUnitButtonClick);
        setExerciseState('create');
    }
    populateExerciseGraph(ordering, exerciseId);
});
